#!/bin/bash

# Create an edge node for developers.

# Usage:
# docker run -t -i -p 80:8000 -p 222:22 edge.eper.io sh
# docker run -t -i -p 80:8000 -p 222:22 -v /home/rancher/git:/root/git:rw -v /home/rancher/.ssh/authorized_keys:/root/.ssh/authorized_keys:ro edge.eper.io sh
# ssh-keygen -q -f /root/.ssh/id_rsa -N '' && cp /root/.ssh/authorized_keys0 /root/.ssh/authorized_keys && chmod a-w /root/.ssh/authorized_keys && chown root /root/.ssh/authorized_keys && /usr/sbin/sshd -h /root/.ssh/id_rsa -o PubkeyAuthentication=yes -o PasswordAuthentication=no -E /var/log/sshd
# Read only external
# git clone http://z.eper.io/default.git
# Read write internal
# git clone root@<ip>:git/default --config core.sshCommand="ssh -i ~/.ssh/id_rsa -p 222"

mkdir abc
cd abc
cat <<EOF >Dockerfile
FROM alpine@sha256:7144f7bab3d4c2648d7e59409f15ec52a18006a128c733fcff20d3a4a54ba44a
RUN apk add git python3 openssh;
RUN mkdir -p /root/git/default
WORKDIR /root/git
# Optional git clone --branch v2.40.0 <original>
RUN git init default
RUN git clone --bare default default.git
WORKDIR /root/git/default.git
RUN git --bare update-server-info
WORKDIR /root/git
CMD sh -c 'nohup /usr/sbin/sshd & python3 -m http.server'
EOF
docker build -t edge.eper.io .
cd ..
rm -rf abc

docker run --restart=always -d -p 80:8000 -p 222:22 -v /home/rancher/git:/root/git:rw -v /home/rancher/.ssh/authorized_keys:/root/.ssh/authorized_keys:ro edge.eper.io



