# eper

## Concept

EPER distributed operating system is the platform for lean devices and cloud services.

The main concept is that each feature cannot be more than thousand lines compared to millions of lines of Kubernetes, Linux, or Windows.

This project was inspired by Raspberry PI and Arduino to create lean software platform that has a very quick development time. Eper stands for strawberry in Hungarian.

## Unique features

- We do not use cookies, so we respect your privacy. No "Allow All..." buttons bother your experience anymore.
- We have a lean thousand line source code per feature, so any developer can be trained to be productive within a day. Guaranteed.
- We favor Englang, Engineering Language that follows the rules of common English to describe API calls, code, data (!), backups, and documentation. There are no more headaches whether a new vendor can handle the binary backup files.
- We are transparent and accountable. Having data files in plain English allows even a tax accountant to audit the raw data.
- We use a unique technology to transfer a thin client window into the browser, so there are no browser dependency issues anymore.
- We do open source Creative Commons Zero most of our codebase. It is well suitable for research organizations that have issues with other open source licenses protecting their own patents. Please follow up with your professional advisor.

## Support
Contact: miklos.szegedi@eper.io

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment
We use the Creative Commons Zero.

## License

This document is Licensed under Creative Commons CC0.
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this document to the public domain worldwide.
This document is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this document.
If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
